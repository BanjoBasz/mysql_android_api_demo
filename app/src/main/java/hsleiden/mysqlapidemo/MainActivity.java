package hsleiden.mysqlapidemo;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;


import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONObject;
/**
 * Created by Jeroen Rijsdijk on 23-3-2016.
 */

public class MainActivity extends AppCompatActivity {

    TextView tv;
    Button getButton,insertButton,deleteButton;

    RequestQueue requestQueue;
    String getUrl = "http://192.168.162.2/showUsers.php";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        tv = (TextView) findViewById(R.id.textView);
        getButton = (Button) findViewById(R.id.getButton);
        insertButton = (Button) findViewById(R.id.insertButton);
        deleteButton = (Button) findViewById(R.id.deleteButtonMain);

        requestQueue = Volley.newRequestQueue(getApplicationContext());

        insertButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this,InsertActivity.class));
            }
        });

        deleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this,DeleteActivity.class));
            }
        });

        getButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, getUrl, (String) null, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try{
                            JSONArray users = response.getJSONArray("users");
                            tv.setText("");

                            for(int i =0; i < users.length(); i ++)
                            {
                                JSONObject user = users.getJSONObject(i);

                                String voornaam = user.getString("voornaam");
                                String achternaam = user.getString("achternaam");
                                String email = user.getString("email");
                                String create_date = user.getString("create_date");

                                tv.append(create_date + "\n" + voornaam + "\n" + achternaam + "\n" + email + "\n" + "\n");
                            }
                        }catch(Exception e)
                        {

                        }

                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        tv.setText("Error: \n\n" + error.toString());
                    }
                }); requestQueue.add(jsonObjectRequest);
            }
        });
    }
}

